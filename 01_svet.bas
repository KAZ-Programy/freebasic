screen 21

const XMAX = 1280' 320
const YMAX = 1024' 240

dim CLR as Ubyte ' color
dim stp as Ubyte ' step
dim ndx as Ubyte ' index

CLR = 0
stp = 25
ndx = 0
do while stp > 0
	line(0, YMAX - ndx * stp)-(XMAX, YMAX - ndx * stp), CLR
	
	stp = stp - 1
	ndx = ndx + 1
	CLR = CLR + 3
loop

Circle(XMAX / 2, YMAX / 2), XMAX / 10, 49 ' x, y, radius, color

Sleep  ' wait for key pressed
GetKey 'clear the keyboard buffer

screen 0 ' Close graphical window
